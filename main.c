#include "Fichier.h"


int main(int argc, char *argv[])

{

  SDL_Surface *ecran = NULL, *rectangle = NULL;

  SDL_Rect position;


  SDL_Init(SDL_INIT_VIDEO);


  ecran = SDL_SetVideoMode(640, 480, 32, SDL_HWSURFACE);

  // Allocation de la surface

  rectangle = SDL_CreateRGBSurface(SDL_HWSURFACE, 220, 180, 32, 0, 0, 0, 0);

  SDL_WM_SetCaption("MAZE", NULL);


  SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format,166 ,166 ,166));

  return menu(ecran,rectangle);

}

char **niveau(int level){

 
  char source[50];
  sprintf(source,"%d",level);
  strcat(source,".txt");	
  printf("%s",source);

  char **tab = lectureFichier(source);
  
  if(tab != NULL){
    return tab;
  }
  else{
    return NULL; 
  }

  

}

int jeu(SDL_Surface *ecran, SDL_Surface *rectangle,int level)
{    
  // Tablau avec le labyrinthe
  char **tab = niveau(level);
  
  if(tab == NULL){
    win(ecran,rectangle);
  }
  else
    {    
      // Image des murs
      SDL_Surface *mur;
      mur=IMG_Load("mur.png");
      SDL_Rect murPosition;

      // Image des sols
      SDL_Surface *sol;
      sol=IMG_Load("sol.png");
      SDL_Rect solPosition;

      // Image des pieges
      SDL_Surface *piege;
      piege=IMG_Load("piege.png");
      SDL_Rect piegePosition;
     
      // Image des escaliers
      SDL_Surface *escalier;
      escalier=IMG_Load("escalier.png");
      SDL_Rect escalierPosition;

      // Image des coeurs
      SDL_Surface *coeur;
      coeur=IMG_Load("heart.png");
      SDL_Rect coeurPosition;

      // Création des murs et sols
      int i,j;
      for(i=0;i<24;i++){

	for(j=0;j<33;j++){

	  // MUR
	  if(tab[i][j]== '#'){	   
	    murPosition.x=j*20;
	    murPosition.y=i*20;
	    SDL_BlitSurface(mur,NULL,ecran,&murPosition);
	 
	  }

	  // SOL
	  else if(tab[i][j] == ' '){
	    solPosition.x=j*20;
	    solPosition.y=i*20;
	    SDL_BlitSurface(sol,NULL,ecran,&solPosition);
	  
	  }

	  // PIEGE
	  else if(tab[i][j] == 'X'){
	    piegePosition.x=j*20;
	    piegePosition.y=i*20;
	    SDL_BlitSurface(piege,NULL,ecran,&piegePosition);
	 
	  }

	  else if(tab[i][j] == 'H'){
	    escalierPosition.x=j*20;
	    escalierPosition.y=i*20;
	    SDL_BlitSurface(escalier,NULL,ecran,&escalierPosition);
	  
	  }
      
	  // VIE
	  else if(tab[i][j] == 'C'){
	    solPosition.x=j*20;
	    solPosition.y=i*20;
	    SDL_BlitSurface(sol,NULL,ecran,&solPosition);

	    coeurPosition.x=j*20;
	    coeurPosition.y=i*20;
	    SDL_BlitSurface(coeur,NULL,ecran,&coeurPosition);

	  
	  }
	}
      }

 
      // Vie du héro
      int vie = 5;
      int k;
	
      // Création du héro
      SDL_Surface *hero;
      hero=IMG_Load("heros.png");
      SDL_Rect heroPosition;
      heroPosition.x=20;
      heroPosition.y=20;
      SDL_BlitSurface(hero,NULL,ecran,&heroPosition);

      // Affichage des murs et des héros
      SDL_Flip(ecran);


      while(1){
      
	// Affichage de la vie  
	for(k=0;k<vie;k++){
	  coeurPosition.x=k*20;
	  coeurPosition.y=0;
	  SDL_BlitSurface(coeur,NULL,ecran,&coeurPosition);
	}

	SDL_Event event;
	SDL_WaitEvent(&event);

	// On sauvegarde la position du héro pour un sol
	solPosition.x=heroPosition.x;
	solPosition.y=heroPosition.y;

	// On sauvegarde la position du héro pour un piège
	piegePosition.x=heroPosition.x;
	piegePosition.y=heroPosition.y;
    
	// Position du héro après déplacement
	int x,y;

	// Position du héro avant déplacement
	int aX,aY;

	aX = heroPosition.x/20;
	aY = heroPosition.y/20;

	// Si l'utilisateur clique sur la croix alors on ferme
	if(event.type == SDL_QUIT){break;}

	// Si l'utilsateur appuie sur une touche
	else if(event.type == SDL_KEYDOWN){
      
	  // Fleche du haut
	  if(event.key.keysym.sym == SDLK_UP){
	 
	    x = heroPosition.x/20;
	    y =(heroPosition.y-20)/20;

	    if(tab[y][x] != '#'){
	      heroPosition.y -= 20;
	     	
	      if(tab[y][x] == 'X'){
		vie = vie-1;
		murPosition.x = vie*20;
		murPosition.y = 0;
		SDL_BlitSurface(mur,NULL,ecran,&murPosition);
		piegePosition.x =heroPosition.x;
		piegePosition.y = heroPosition.y;
		SDL_BlitSurface(piege,NULL,ecran,&piegePosition);
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);
	      }
	      else if(tab[aY][aX] == 'X'){
		SDL_BlitSurface(piege,NULL,ecran,&piegePosition);
	      }

	      else if(tab[y][x] == 'H'){
		escalierPosition.x =heroPosition.x;
		escalierPosition.y = heroPosition.y;
		SDL_BlitSurface(escalier,NULL,ecran,&escalierPosition);
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);
		level ++;
		jeu(ecran,rectangle,level);
		exit(EXIT_SUCCESS);
	      }
	      else if(tab[aY][aX] == 'H'){
		SDL_BlitSurface(escalier,NULL,ecran,&escalierPosition);
	      }
		
		
	      else{
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);
	      }
	
	      SDL_BlitSurface(hero,NULL,ecran,&heroPosition);
	      SDL_Flip(ecran);
	    }
	  }
	  // Fleche du bas
	  else if(event.key.keysym.sym == SDLK_DOWN){
	  
	    x = heroPosition.x/20;
	    y = (heroPosition.y+20)/20;

	    if(tab[y][x] != '#'){
	      heroPosition.y += 20;
	    
	      if(tab[y][x] == 'X'){
		vie = vie-1;
		murPosition.x = vie*20;
		murPosition.y = 0;
		SDL_BlitSurface(mur,NULL,ecran,&murPosition);
		piegePosition.x =heroPosition.x;
		piegePosition.y = heroPosition.y;
		SDL_BlitSurface(piege,NULL,ecran,&piegePosition);
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);
	      }
	      else if(tab[aY][aX] == 'X'){
		SDL_BlitSurface(piege,NULL,ecran,&piegePosition);
	      }

	      else if(tab[y][x] == 'H'){
		escalierPosition.x =heroPosition.x;
		escalierPosition.y = heroPosition.y;
		SDL_BlitSurface(escalier,NULL,ecran,&escalierPosition);
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);
		level ++;
		jeu(ecran,rectangle,level);
		exit(EXIT_SUCCESS);
	      }
	      else if(tab[aY][aX] == 'H'){
		SDL_BlitSurface(escalier,NULL,ecran,&escalierPosition);
	      }
 
	      else{
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);
	      }
	
	      SDL_BlitSurface(hero,NULL,ecran,&heroPosition);
	      SDL_Flip(ecran);
	    }
	  }
	  // Fleche de droite
	  else if(event.key.keysym.sym == SDLK_RIGHT){
	   
	    x = (heroPosition.x +20)/20;
	    y = heroPosition.y/20;
	   
	    if(tab[y][x] != '#'){
	      heroPosition.x += 20;
	   
	      if(tab[y][x] == 'X'){
		vie = vie-1;
		murPosition.x = vie*20;
		murPosition.y = 0;
		SDL_BlitSurface(mur,NULL,ecran,&murPosition);
		piegePosition.x =heroPosition.x;
		piegePosition.y = heroPosition.y;
		SDL_BlitSurface(piege,NULL,ecran,&piegePosition);
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);
	      }
	      else if(tab[aY][aX] == 'X'){
		SDL_BlitSurface(piege,NULL,ecran,&piegePosition);
	      }

	      else if(tab[y][x] == 'H'){
		escalierPosition.x =heroPosition.x;
		escalierPosition.y = heroPosition.y;
		SDL_BlitSurface(escalier,NULL,ecran,&escalierPosition);
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);
		level ++;
		jeu(ecran,rectangle,level);
		exit(EXIT_SUCCESS);
	      }
	      else if(tab[aY][aX] == 'H'){
		SDL_BlitSurface(escalier,NULL,ecran,&escalierPosition);
	      }

	      else{
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);
	      }
	
	      SDL_BlitSurface(hero,NULL,ecran,&heroPosition);
	      SDL_Flip(ecran);
	    }
	   
	  }
	  // FLeche de gauche
	  else if(event.key.keysym.sym == SDLK_LEFT){

	    x = (heroPosition.x - 20)/20;
	    y = heroPosition.y/20;

	    if(tab[y][x] != '#'){
	      heroPosition.x -= 20;
	   
	      if(tab[y][x] == 'X'){
		vie = vie-1;
		murPosition.x = vie*20;
		murPosition.y = 0;
		SDL_BlitSurface(mur,NULL,ecran,&murPosition);
		piegePosition.x =heroPosition.x;
		piegePosition.y = heroPosition.y;
		SDL_BlitSurface(piege,NULL,ecran,&piegePosition);
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);
	      }
	      else if(tab[aY][aX] == 'X'){
		SDL_BlitSurface(piege,NULL,ecran,&piegePosition);
	      }

	      else if(tab[y][x] == 'H'){
		escalierPosition.x =heroPosition.x;
		escalierPosition.y = heroPosition.y;
		SDL_BlitSurface(escalier,NULL,ecran,&escalierPosition);
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);

		level ++;
		jeu(ecran,rectangle,level);
		exit(EXIT_SUCCESS);
	      }
	      else if(tab[aY][aX] == 'H'){
		SDL_BlitSurface(escalier,NULL,ecran,&escalierPosition);
	      }


         
	      else{
		SDL_BlitSurface(sol,NULL,ecran,&solPosition);
	      }
	
	      SDL_BlitSurface(hero,NULL,ecran,&heroPosition);
	      SDL_Flip(ecran);
	    }
	  }

	  if(tab[y][x] == 'C'){
	    tab[y][x] = ' ';
	    vie++;
	  }

	}
     
	SDL_Flip(ecran);

	if(vie <= 0){
	  over(ecran,rectangle);
	  exit(EXIT_SUCCESS);
	}

      }
      SDL_FreeSurface(coeur);
      SDL_FreeSurface(mur);
      SDL_FreeSurface(escalier);
      SDL_FreeSurface(piege);
      SDL_FreeSurface(sol);
      SDL_FreeSurface(hero);
      SDL_Quit();
    }

  return EXIT_SUCCESS;


}


int menu(SDL_Surface *ecran, SDL_Surface *rectangle)
{

  SDL_Surface *fenetre =ecran;
  SDL_Surface *rect = rectangle;
  // Initialisation de font
  TTF_Init();
 


  // Création de la police
  TTF_Font *font;
  font=TTF_OpenFont("Maze.ttf",150);
  SDL_Color fontBlack = {255,255,255};
  SDL_Color fontRed = {219,0,0};

  // création de la surface
  SDL_Surface *jouer;
  jouer=TTF_RenderText_Solid(font,"Jouer",fontRed);
  SDL_Rect jouerPosition;
  
  jouerPosition.x = 150;
  jouerPosition.y = 75;

  SDL_BlitSurface(jouer,NULL,ecran,&jouerPosition);

  SDL_Flip(ecran);
    
  while(1){

    SDL_Event event;
    SDL_WaitEvent(&event);

    if(event.type == SDL_QUIT){break;}
    else if(event.type == SDL_MOUSEBUTTONDOWN){

      if(event.button.y > 75 && event.button.y < 200 &&
	 event.button.x > 150 && event.button.x < 520)
	{
	 ecran = SDL_SetVideoMode(640, 480, 32, SDL_HWSURFACE);
	  // Allocation de la surface

	  rectangle = SDL_CreateRGBSurface(
		     SDL_HWSURFACE, 220, 180, 32, 0, 0, 0, 0);
	  SDL_WM_SetCaption("MAZE", NULL);
	  SDL_FillRect(ecran, NULL, 
		      SDL_MapRGB(ecran->format,166 ,166 ,166));
	  jeu(ecran,rectangle,1); 
	  exit(EXIT_SUCCESS);
	}
    }
    else if(event.type == SDL_MOUSEMOTION)
      {
	 if(event.button.y > 75 && event.button.y < 200 &&
	 event.button.x > 150 && event.button.x < 520)
	{
	   jouer=TTF_RenderText_Solid(font,"Jouer",fontRed);
	   SDL_BlitSurface(jouer,NULL,ecran,&jouerPosition);
	   SDL_Flip(ecran);
	}
	 else{
	   jouer=TTF_RenderText_Solid(font,"Jouer",fontBlack);
	   SDL_BlitSurface(jouer,NULL,ecran,&jouerPosition);
	   SDL_Flip(ecran);
	 }

    }

    SDL_Flip(ecran);
  }

  SDL_FreeSurface(jouer);
  TTF_Quit();

}



int over(SDL_Surface *ecran, SDL_Surface *rectangle)
{

  SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format,0 ,0 ,0));
  SDL_Flip(ecran);

  // Initialisation de font
  TTF_Init();
 


  // Création de la police
  TTF_Font *font;
  font=TTF_OpenFont("Maze.ttf",135);
  SDL_Color fontBlack = {255,255,255};
  SDL_Color fontRed = {219,0,0};

  // création de la surface
  SDL_Surface *jouer;
  jouer=TTF_RenderText_Solid(font,"Game Over",fontRed);
  SDL_Rect jouerPosition;
  
  jouerPosition.x = 25;
  jouerPosition.y = 130;

  SDL_BlitSurface(jouer,NULL,ecran,&jouerPosition);

  SDL_Flip(ecran);
    
  while(1){

    SDL_Event event;
    SDL_WaitEvent(&event);

    if(event.type == SDL_QUIT){break;}
    
     else if(event.type == SDL_MOUSEBUTTONDOWN){

      if(event.button.y > 130 && event.button.y < 200 &&
	 event.button.x > 25 && event.button.x < 620)
	{

	 ecran = SDL_SetVideoMode(640, 480, 32, SDL_HWSURFACE);
	  // Allocation de la surface

	  rectangle = SDL_CreateRGBSurface(
		     SDL_HWSURFACE, 220, 180, 32, 0, 0, 0, 0);
	  SDL_WM_SetCaption("MAZE", NULL);
	  SDL_FillRect(ecran, NULL, 
		      SDL_MapRGB(ecran->format,166 ,166 ,166));
	  menu(ecran,rectangle);
	  exit(EXIT_SUCCESS);

	}
    }
   

    SDL_Flip(ecran);
  }

  SDL_FreeSurface(jouer);
  TTF_Quit();

}

int win(SDL_Surface *ecran, SDL_Surface *rectangle)
{

  SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format,0 ,0 ,0));
  SDL_Flip(ecran);

  // Initialisation de font
  TTF_Init();
 


  // Création de la police
  TTF_Font *font;
  font=TTF_OpenFont("Maze.ttf",135);
  SDL_Color fontBlack = {255,255,255};
  SDL_Color fontRed = {219,0,0};

  // création de la surface
  SDL_Surface *jouer;
  jouer=TTF_RenderText_Solid(font,"YOU WIN",fontBlack);
  SDL_Rect jouerPosition;
  
  jouerPosition.x = 100;
  jouerPosition.y = 130;

  SDL_BlitSurface(jouer,NULL,ecran,&jouerPosition);

  SDL_Flip(ecran);
    
  while(1){

    SDL_Event event;
    SDL_WaitEvent(&event);

    if(event.type == SDL_QUIT){break;}
    
     else if(event.type == SDL_MOUSEBUTTONDOWN){

      if(event.button.y > 130 && event.button.y < 200 &&
	 event.button.x > 100 && event.button.x < 580)
	{

	 ecran = SDL_SetVideoMode(640, 480, 32, SDL_HWSURFACE);
	  // Allocation de la surface

	  rectangle = SDL_CreateRGBSurface(
		     SDL_HWSURFACE, 220, 180, 32, 0, 0, 0, 0);
	  SDL_WM_SetCaption("MAZE", NULL);
	  SDL_FillRect(ecran, NULL, 
		      SDL_MapRGB(ecran->format,166 ,166 ,166));
	  menu(ecran,rectangle);
	  exit(EXIT_SUCCESS);

	}
    }
   

    SDL_Flip(ecran);
  }

  SDL_FreeSurface(jouer);
  TTF_Quit();

}



