all : main

main : main.o Fichier.o
	gcc -o main main.o Fichier.o -lSDL -lSDL_image -lSDL_ttf

main.o : main.c 
	gcc -c main.c 

Fichier.o : Fichier.c
	gcc -c Fichier.c

clean :
	rm main.o Fichier.o main	
